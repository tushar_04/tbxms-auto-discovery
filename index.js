const { Builder, By, Key, until } = require('selenium-webdriver');

async function addDevice() {
    let driver = await new Builder().forBrowser('chrome').build();


    await driver.get('http://192.168.1.32:8980/opennms/admin/discovery/edit-scan.jsp');
    const mainWindowHandle = await driver.getWindowHandle();
    await driver.manage().window().maximize();
    let input1 = await driver.findElement(By.id('input_j_username'));
    await input1.sendKeys('admin');
    let input2 = await driver.findElement(By.id('input_j_password'));
    await input2.sendKeys('admin', Key.RETURN);
    let foreign = await driver.findElement(By.id('foreignsource'));
    await foreign.sendKeys('devXMS');

    // // Click the "addspecific" button to open the pop-up window
    let addSpecific = await driver.findElement(By.xpath("//button[@onclick='addSpecific();']"));
    await addSpecific.click();
    let focus_specific = await driver.getAllWindowHandles();
    await driver.switchTo().window(focus_specific[1]);
    let addSpecificip = await driver.findElement(By.id('ipaddress'));
    await addSpecificip.sendKeys('192.168.1.80');
    let addSpecificforeign = await driver.findElement(By.id('foreignsource'));
    await addSpecificforeign.sendKeys('devXMS');
    let addSpecificbutton = await driver.findElement(By.id('addSpecific'));
    await addSpecificbutton.click();
    await driver.switchTo().window(focus_specific[0]);//remove focus from addspecific

    // Click the "addInclude" button to open the pop-up window
    // let addIncludeurl = await driver.findElement(By.xpath("//button[@onclick='addIncludeUrl();']"));
    // await addIncludeurl.click();
    // let focus_addincludeurl = await driver.getAllWindowHandles();
    // await driver.switchTo().window(focus_addincludeurl[1]);
    // let addincludeurlIP = await driver.findElement(By.id('url'));
    // await addincludeurlIP.sendKeys('http://192.168.1.80/lou/url.txt');
    // let addincludeforeign = await driver.findElement(By.id('foreignsource'));
    // await addincludeforeign.sendKeys('devXMS');
    // let addIncludeurlButton = await driver.findElement(By.id('addIncludeUrl'));
    // await addIncludeurlButton.click();
    // await driver.switchTo().window(focus_addincludeurl[0]);//remove focus from focus_addincludeurl



    // //** addIncludeRange() */
    // let addIncludeRange = await driver.findElement(By.xpath('//*[@id="modifyDiscoveryConfig"]/div[4]/div/div/div[3]/button'));
    // await addIncludeRange.click();
    // let focus_addincluderange = await driver.getAllWindowHandles();
    // await driver.switchTo().window(focus_addincluderange[1]);
    // let addirstart = await driver.findElement(By.id('base'));
    // await addirstart.sendKeys('192.168.1.40');
    // let addirend = await driver.findElement(By.id('end'));
    // await addirend.sendKeys('192.168.1.40');
    // let irTimeout = await driver.findElement(By.id('timeout'));
    // await irTimeout.sendKeys('2000');
    // let irRetries = await driver.findElement(By.id('retries'));
    // await irRetries.sendKeys('1');
    // let irforeign = await driver.findElement(By.id('foreignsource'));
    // await irforeign.sendKeys('devXMS');
    // let addIncluderangeButton = await driver.findElement(By.id('addIncludeRange'));
    // await addIncluderangeButton.click();
    // await driver.switchTo().window(focus_addincluderange[0]);


    //Click addExcludeRange button to open the opener of addExcludeRange
    // let addExcludeRange = await driver.findElement(By.xpath("//button[@onclick='addExcludeRange();']"));
    // await addExcludeRange.click();
    // let focus_addexcluderange = await driver.getAllWindowHandles();
    // await driver.switchTo().window(focus_addexcluderange[1]);
    // let adderstart = await driver.findElement(By.id('begin'));
    // await adderstart.sendKeys('192.168.1.30');
    // let adderend = await driver.findElement(By.id('end'));
    // await adderend.sendKeys('192.168.1.30');
    // let addexcluderangeButton = await driver.findElement(By.id('addExcludeRange'));
    // await addexcluderangeButton.click();
    // await driver.switchTo().window(focus_addexcluderange[0]);//remove focus from focus_addexcluderange


    //Restart discovery  
    let input5 = driver.wait(until.elementLocated(By.xpath('//*[@id="modifyDiscoveryConfig"]/button[3]')));
    await input5.click();
    console.log('Device added successfully');

    // driver.close();
    await driver.quit();
}

addDevice();

